package com.ust.RecapEx;

class A
{
	int a=100;
}
class B extends A{
	int a=200;
	void display() {
		System.out.println(a);
		System.out.println(super.a);
	
		
	}
}

public class SuperKey extends B{
	int a=300;
	void display() {
		System.out.println(a);
		System.out.println(super.a);
		
	}
	public static void main(String[] args) {
		new SuperKey().display();
		new B().display();
	}

}
