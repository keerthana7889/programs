package com.ust.RecapEx;

public class StaticEx {
	
	public int id;
	public String name;
	
	
	public static String company="Ust";
	public StaticEx(int id , String name) {
		super();
		this.id=id;
		this.name=name;
	}
	public void display() {
		System.out.println(id +" "+name + " " + company);
	}

	public static void main(String[] args) {
		StaticEx  soney= new StaticEx(101, "soney");
	soney.display();
	
	StaticEx.company="UST";
		
	StaticEx  wilson= new StaticEx(121, "wilson");
	wilson.display();
		
	StaticEx  justin= new StaticEx(131, "justin");
	justin.display();
		
		
		
		// TODO Auto-generated method stub
		

	}

}